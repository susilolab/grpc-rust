# grpc-rust

Contoh aplikasi sederhana dengan rust sebagai gRPC server dan PHP sebagai gRPC client.

# Generate PHP client stub classes

```bash
PROTOC=/usr/bin/protoc
PLUGIN=protoc-gen-grpc=grpc-plugin/grpc_php_plugin

$PROTOC --proto_path=proto/helloworld \
       --php_out=app \
       --grpc_out=app \
       --plugin=$PLUGIN proto/helloworld/helloworld.proto
```
